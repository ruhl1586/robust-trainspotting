use eventsim::{Process, ProcessState, EventId};
use super::infrastructure::*;
use input::staticinfrastructure::*;
use smallvec::SmallVec;
use super::dynamics::*;
use output::history::TrainLogEvent;
use super::Sim;
use std::cmp;
use railway::distribution::{MaxSpeedDistribution, MaxSpeedExponentialDistribution};
use railway::distribution::DistributionType;
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::fs::File;
use std::io::prelude::*;

enum ModelContainment {
    Inside,
    Outside,
}

enum Activation {
    Wait(EventId),
    Activate,
    Running,
}

#[derive(Debug)]
struct Train {
    location: (NodeId, (Option<NodeId>, f64)),
    velocity: f64,
    params: TrainParams,
    under_train: SmallVec<[(NodeId, f64); 4]>,
    last_location_id: NodeId,
    last_max_vel: f64,
    distribution: Box<dyn MaxSpeedDistribution>,
}

pub struct Driver {
    id :usize,
    train: Train,
    authority: f64,
    step: (DriverAction, f64),
    connected_signals: SmallVec<[(ObjectId, f64); 4]>,
    logger: Box<Fn(TrainLogEvent)>,
    activation: Activation,
    timestep: Option<f64>,
    visits: Vec<NodeId>,
    stochastic: bool,
    visits_with_distance: HashMap<NodeId, f64>,
    last_time_of_visit: f64,
    last_required_tmin: f64,
    last_delay: f64,
    succeeded_delays: usize,
    total_delays: usize,
    delay_statistics: Vec<(usize, f64, f64)>, //nodeId, required delay, realized delay
    required_delay_stats: Vec<(usize, f64, f64, f64, f64, f64)> // nodeid, required delay, distance, min time, current speed, new speed
}

impl Driver {
    pub fn new(sim: &mut Sim,
               id :usize,
               activated: EventId,
               node: NodeId,
               auth: f64,
               params: TrainParams,
               logger: Box<Fn(TrainLogEvent)>,
               timestep: Option<f64>,
               visits: Vec<NodeId>,
                stochastic: bool,
               visits_with_distance: HashMap<NodeId, f64>)
               -> Self {

        //construct MaxSpeedDistribution from TrainParams
        let mut max_speed_distribution: Box<dyn MaxSpeedDistribution>;

        match params.distribution {
            DistributionType::ExponentialDistribution(chance, lambda) => {
                //print!("Driver {} drawing from Dist with Chance: {}, Mean: {}, Std_dev: {}\n", id, chance, mean, std_dev);
                max_speed_distribution = Box::new(MaxSpeedExponentialDistribution::new(chance, lambda));
            },
            _ => panic!("Unknown Distribution")
        }

       //println!("INITIAL AUTHORITY {:?}", auth);
        let train = Train {
            params: params,
            location: (0, (Some(node),0.0)),
            velocity: 0.0,
            under_train: SmallVec::new(),
            last_location_id: 0,
            last_max_vel: params.max_vel,
            distribution: max_speed_distribution
        };

        let d = Driver {
            id: id,
            train: train,
            authority: auth - 20.0,
            step: (DriverAction::Coast, *sim.time()),
            connected_signals: SmallVec::new(),
            logger: logger,
            activation: Activation::Wait(activated),
            timestep: timestep,
            visits: visits,
            stochastic: stochastic,
            visits_with_distance: visits_with_distance,
            last_time_of_visit: 0.0,
            last_required_tmin: 0.0,
            last_delay: 0.0,
            succeeded_delays: 0,
            total_delays: 0,
            delay_statistics: vec![],
            required_delay_stats: vec![]
        };

        d
    }

    fn activate(&mut self, sim:&mut Sim) {
        if *sim.time() > 0.0 {
            (self.logger)(TrainLogEvent::Wait(*sim.time()));
        }
        self.step = (DriverAction::Coast, *sim.time());
        self.move_train_discrete(sim);
    }

    fn goto_node(&mut self, sim: &mut Sim, node: NodeId) {
        //println!("TRAIN goto node {}", node);
        for obj in sim.world.statics.nodes[node].objects.clone() {
            if let Some(p) = sim.world.statics.objects[obj].arrive_front(node, self.id) {
                sim.start_process(p);
            }
            self.arrive_front(sim, obj);
        }
        self.train.under_train.push((node, self.train.params.length));
    }

    fn arrive_front(&mut self, sim: &Sim, obj: ObjectId) {
        match sim.world.statics.objects[obj] {
            StaticObject::Sight { distance, signal } => {
                self.connected_signals.push((signal, distance));
                (self.logger)(TrainLogEvent::Sight(signal,true));
            }
            StaticObject::Signal { .. } => {
                let log = &mut self.logger;
                self.connected_signals.retain(|&mut (s, _d)| {
                    let lost = s == obj;
                    if lost { log(TrainLogEvent::Sight(s,false)); }
                    !lost
                });
            }
            _ => {}
        }
    }

    fn move_train(&mut self, sim: &mut Sim) -> ModelContainment {
        let dt = *sim.time() - self.step.1;
        if dt <= 1e-5 {
            return ModelContainment::Inside;
        }

        self.move_train_continuous(sim);
        self.move_train_discrete(sim);

        if (self.train.location.1).0.is_none() && self.train.under_train.len() == 0 {
            ModelContainment::Outside
        } else {
            ModelContainment::Inside
        }
    }

    fn move_train_continuous(&mut self, sim :&mut Sim) {
        let (action, action_time) = self.step;
        let dt = *sim.time() - action_time;
        let update = dynamic_update(&self.train.params, self.train.velocity, 
                                    DriverPlan { action: action, dt: dt, });

        //println!("DYNAMIC UPDATE {:?}", (action,dt));
        //println!("{:?}", update);

        (self.logger)(TrainLogEvent::Move(dt, action, update));
        self.train.velocity = update.v;
        //println!("train loc {:?}", self.train.location);
        (self.train.location.1).1 -= update.dx;
        //println!("train loc {:?}", self.train.location);

        // In case there are no signals in sight,
        // the remembered authority is updated.
        self.authority -= update.dx;

        let id = self.id;
        self.train.under_train.retain(|&mut (node, ref mut dist)| {
            *dist -= update.dx;
            if *dist < 1e-5 {
                // Cleared a node.
                
                for obj in sim.world.statics.nodes[node].objects.clone() {
                    if let Some(p) = sim.world.statics.objects[obj].arrive_back(node, id) {
                        sim.start_process(p);
                    }
                }

                false
            } else {
                true
            }
        });

        {
        let log = &mut self.logger;
        self.connected_signals.retain(|&mut (obj, ref mut dist)| {
            *dist -= update.dx;
            let lost = *dist < 10.0; // If closer than 10 m, signal should already be green
                                     // and seeing a red for a very short time should be because
                                     // detector is placed in front of signal and this should not 
                                     // bother the driver.
            if lost { log(TrainLogEvent::Sight(obj, false)); } 
            !lost
        });
        }
    }

    fn move_train_discrete(&mut self, sim :&mut Sim) {
        loop {
            let (_, (end_node, dist)) = self.train.location;
            if dist > 1e-5 || end_node.is_none() { break; }

            let new_start = sim.world.statics.nodes[end_node.unwrap()].other_node;
            (self.logger)(TrainLogEvent::Node(end_node.unwrap()));
            self.goto_node(sim, new_start);
            (self.logger)(TrainLogEvent::Node(new_start));
            match sim.world.edge_from(new_start) {
                Some((Some(new_end_node), d)) => {
                    self.train.location = (new_start, (Some(new_end_node), d));
                    (self.logger)(TrainLogEvent::Edge(new_start, Some(new_end_node)));
                }
                Some((None, d)) => {
                    self.train.location = (new_start, (None, d));
                    (self.logger)(TrainLogEvent::Edge(new_start, None));
                }
                None => panic!("Derailed"),
            }
        }
    }

    fn plan_ahead(&mut self, sim: &Sim) -> DriverPlan {
        // Travel distance is limited by next node
        //println!("Travel distance is limited by next node");
        //println!("{:?}", (self.train.location.1).1);
        let mut max_dist = (self.train.location.1).1;

        // Travel distance is limited by nodes under train
        //println!("Travel distance is limited by nodes under train");
        //println!("{:?}", self.train.under_train);
        for &(_n, d) in self.train.under_train.iter() {
            max_dist = max_dist.min(d);
        }

        // Travel distance is limited by sight distances
        //println!("Travel distance is limited by sight distances");
        //println!("{:?}", self.connected_signals);
        for &(_n, d) in self.connected_signals.iter() {
            max_dist = max_dist.min(d);
        }

        // Authority is updated by signals
        for &(sig, dist) in self.connected_signals.iter() {
            match sim.world.state[sig] {
                ObjectState::Signal { ref authority } => {
                    match *authority.get() {
                        (Some(auth_dist), distant_sig) => {
                            //println!("Signal green in sight dist{} sigauth{} self.auth{}", dist, d, dist+d-20.0);
                            self.authority = dist + auth_dist + distant_sig.unwrap_or(0.0) - 20.0;
                            if self.authority < 0.0 { self.authority = 0.0; }
                        }
                        (None,_) => {
                            //println!("Signal red in sight dist{} self.auth{}", dist,dist-20.0);
                            self.authority = dist - 20.0;
                            if self.authority < 0.0 { self.authority = 0.0; }
                            break;
                        }
                    }
                }
                _ => panic!("Not a signal"),
            }
        }

        //println!("Updated authority {}", self.authority);

        // Static maximum speed profile ahead from current position
        // TODO: other speed limitations

        // @Bjørnar this is where I would like to restrict the speed
        // Speed change when new Node was passed:
        let mut local_max_velocity = self.train.last_max_vel;

        // only change speed if this is a stochastic simulation
        if self.stochastic{
            //print!("All visits:  {:?}\n", self.visits);
            //print!("location change detected: from {} to {}\n", self.train.last_location_id, self.train.location.0);
            //print!("Current v: {}\n", self.train.velocity);


            if self.visits.contains(&self.train.location.0) && self.visits_with_distance.contains_key(&self.train.location.0){
                let upcoming_distance = self.visits_with_distance.get(&self.train.location.0).expect("Visit Not Found in distance List");
                if self.train.location.0 != self.train.last_location_id {
                    // Check if last delay was fulfilled, excluding first visit (no delay until now):
                    if !(self.train.location.0 == *self.visits.first().unwrap()) {
                        let time_passed = *sim.time() - self.last_time_of_visit;
                        let real_delay =  time_passed-self.last_required_tmin;
                        self.delay_statistics.push((self.train.location.0, self.last_delay, real_delay));
                        if real_delay - 10.0 <= self.last_delay && self.last_delay <= real_delay + 10.0 {
                            self.succeeded_delays += 1;
                        }
                        self.total_delays += 1;
                    }
                    self.last_time_of_visit = *sim.time(); // reset visit time to now

                    if upcoming_distance > &0.0 {

                        let (v_for_delay, t_min, delay) = self.train.distribution.draw_new_speed_by_delay_time(
                            self.train.params.max_vel,
                            self.train.params.max_acc,
                            self.train.params.max_brk,
                            *upcoming_distance,
                            f64::max(0.5, self.train.velocity)
                        );
                        local_max_velocity = v_for_delay;
                        // nodeid, required delay, distance, min time, current speed, new speed
                        self.required_delay_stats.push((self.train.location.0, delay, *upcoming_distance, t_min, self.train.velocity, v_for_delay));
                        self.train.last_max_vel = local_max_velocity;
                        self.last_required_tmin = t_min;
                        self.last_delay = delay;
                    }
                }

                //print!("New max v: {}\n", local_max_velocity);
            }
            self.train.last_location_id = self.train.location.0; // set this location as the last location
        }

        let static_speed_profile = StaticMaximumVelocityProfile {
            local_max_velocity: local_max_velocity,
            max_velocity_ahead: SmallVec::from_slice(&[DistanceVelocity {
               dx: self.authority, v: 0.0}]),
        };

        let plan = dynamic_plan_step(&self.train.params,
                          max_dist,
                          self.train.velocity,
                          &static_speed_profile);

        //println!("PLAN: {:?} {:?} {:?} {:?} {:?} ", self.train.params, max_dist, self.train.velocity, static_speed_profile,plan);
        plan
    }
}

impl<'a> Process<Infrastructure<'a>> for Driver {
    fn resume(&mut self, sim: &mut Sim) -> ProcessState {
        match self.activation {
            Activation::Wait(ev) => {
                self.activation = Activation::Activate;
                return ProcessState::Wait(SmallVec::from_slice(&[ev]));
            },
            Activation::Activate => {
                self.activate(sim);
                self.activation = Activation::Running;
            },
            Activation::Running => { }
        };

        //println!("resume train");
        let modelcontainment = self.move_train(sim);
        match modelcontainment {
            ModelContainment::Outside => {
                /*if self.stochastic {
                    //let delay_success = format!("Driver {}, {} delays, success rate with error margin 2: {}", self.id, self.total_delays, self.succeeded_delays as f64/self.total_delays as f64);
                    //println!("{}", delay_success);

                    // Store information about successful delay introduction
                    //let csv_string = format!("{}, {}, {}, {}", self.id, self.total_delays, self.succeeded_delays, self.succeeded_delays as f64/self.total_delays as f64 );

                    let mut file = OpenOptions::new()
                        .write(true)
                        .append(true)
                        .create(true)
                        .open(format!("delay_info_real_{}.csv", self.id))
                        .unwrap();
                    for (node, required_delay, real_delay) in &self.delay_statistics {
                        let csv_string = format!{"{}, {}, {}", node, required_delay, real_delay};
                        if let Err(e) = writeln!(file, "{}", csv_string) {
                            eprintln!("Couldn't write to file: {}", e);
                        }
                    }

                    let mut file2 = OpenOptions::new()
                        .write(true)
                        .append(true)
                        .create(true)
                        .open(format!("delay_info_required_{}.csv", self.id))
                        .unwrap();

                    // nodeid, required delay, distance, min time, current speed, new speed
                    for (node, required_delay, distance, t_min, v0, v1) in &self.required_delay_stats {
                        let csv_string = format!{"{}, {}, {}, {}, {}, {}", node, required_delay, distance, t_min, v0, v1};
                        if let Err(e) = writeln!(file2, "{}", csv_string) {
                            eprintln!("Couldn't write to file: {}", e);
                        }
                    }
                }*/
                ProcessState::Finished
            },

            ModelContainment::Inside => {
                let plan = self.plan_ahead(sim);
                self.step = (plan.action, *sim.time());
                    //println!("PLAN  {:?}", plan);

                let mut events = SmallVec::new();
                if plan.dt > 1e-5 {
                    let dt = match self.timestep {
                        Some(m) => if m < plan.dt && plan.dt.is_normal() { m } else { plan.dt },
                        None => plan.dt,
                    };
                    //println!("SET TIMOUT {:?} {:?}", plan.dt, dt);
                    events.push(sim.create_timeout(dt));
                } else {
                    if self.train.velocity > 1e-5 { panic!("Velocity, but no plan."); }
                    self.train.velocity = 0.0;
                    self.step.0 = DriverAction::Coast;
                }
                //println!("Connected signals: {:?}", self.connected_signals);
                for &(ref sig, _) in self.connected_signals.iter() {
                    match sim.world.state[*sig] {
                        ObjectState::Signal { ref authority } => events.push(authority.event()),
                        _ => panic!("Object is not a signal"),
                    }
                }
                ProcessState::Wait(events)
            }
        }
    }
}
