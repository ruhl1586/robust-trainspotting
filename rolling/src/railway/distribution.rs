extern crate rand_distr;
extern crate rand;
extern crate serde;

use self::rand_distr::{Normal, Exp, Distribution};
use self::rand::{Rng};
use self::rand::rngs::OsRng;
use self::serde::{Serialize,Deserialize};


/// Enum holding distribution information, for sending between threads
#[derive(Copy, Clone, Debug)]
#[derive(Serialize,Deserialize)]
pub enum DistributionType {
    NormalDistribution(f32, f32, f32), // (chance, mean, std_dev)
    ExponentialDistribution(f32, f32) // (chance, lambda)
}

/// Interface for any speed-based delay distribution
pub trait MaxSpeedDistribution: std::fmt::Debug {
    // Randomly choose whether a new value should be drawn, return that or original value
    fn draw_new_speed(&mut self, original: f64) -> f64;
    fn draw_new_speed_by_delay_time(&mut self, vmax:f64, accel: f64, braking: f64, distance: f64, current: f64) -> (f64, f64, f64); // speed, normal time, delay
}

/// Speed Delays distributed over an Exponential Distribution
/// P(dv) = chance * Exp(lambda)
#[derive(Copy, Clone, Debug)]
pub struct MaxSpeedExponentialDistribution {
    lambda: f32, // Exponential Distribution lambda value
    chance: f32, // Chance with which a new value is drawn
    distribution: Exp<f64>, // The Normal Distribution object the values are drawn from
}

impl MaxSpeedExponentialDistribution {
    pub fn new(chance: f32, lambda: f32 ) -> MaxSpeedExponentialDistribution{
        MaxSpeedExponentialDistribution {
            lambda,
            chance,
            distribution: Exp::new(lambda as f64).unwrap(),
        }
    }
}

impl MaxSpeedDistribution for MaxSpeedExponentialDistribution {
    fn draw_new_speed(&mut self, original: f64) -> f64 {

        // randomly choose if a new value is to be drawn
        if OsRng.gen_bool(self.chance as f64) {
            // draw and return the new value from the Normal Distribution
            return f64::max(1.0, 60.0*self.distribution.sample(&mut OsRng));
        }

        // if no new value should be drawn return the original value
        original
    }

    fn draw_new_speed_by_delay_time(&mut self, vmax:f64, accel: f64, braking: f64, distance: f64, current: f64) -> (f64, f64, f64) {
        // randomly choose if a new value is to be drawn
        if OsRng.gen_bool(self.chance as f64) {
            // draw a delay time >= 1.0 min from the Exponential Distribution
            // multiply drawn time by 60 to get delay in seconds
            let delay = f64::max(60.0, 60.0*self.distribution.sample(&mut OsRng));

            // return the speed that will lead to this delay
            let (v_new, t_min) = get_speed_by_delay(vmax, accel, braking, distance, current, delay);
            return (v_new, t_min, delay)
        }
        // If no delay was drawn return maximum speed
        (vmax, ((vmax-current)/accel) + ((distance-(0.5*accel*((vmax-current)/accel)*((vmax-current)/accel)+current*((vmax-current)/accel)))/vmax), 0.0)

    }
}

/// Calculate a new speed given a delay time
///
/// if train needs to brake (would travel the distance faster than required with current speed:
///     v_new = f64::max(1.0, -brake * t + vc + sqrt((-brake)*(-brake)*t*t-2*a*dx+2*a*t*vc));
/// else train needs to accelerate (would travel the distance slower than required with current speed):
///     v_new = f64::min(vmax, accel * t + vc - sqrt(accel*accel*t*t-2*a*dx+2*a*t*vc));
///
/// vmax: Maximum speed the train can achieve
/// accel: Fixed Acceleration of the train
/// brake: Fixed Deceleration of the train
/// dx: Distance over which the train should be delayed
/// vc: Current velocity of the train
/// delay: The time the train should take longer compared to travelling the distance with vmax
fn get_speed_by_delay(vmax:f64, accel:f64, brake:f64, dx:f64, vc:f64, delay:f64) -> (f64, f64) { // new speed, t_min
    let t_min = ((vmax-vc)/accel) + ((dx-(0.5*accel*((vmax-vc)/accel)*((vmax-vc)/accel)+vc*((vmax-vc)/accel)))/vmax);
    let t = t_min + delay;
    let tc = dx / vc;
    //print!("Next travel of distance {} should take {} time units\n", dx, t);

    if tc <= t {
        if ((-brake)*(-brake)*t*t-2.0*(-brake)*dx+2.0*(-brake)*t*vc) < 0.0 {
            return (1.0, t_min)
        } else {
            let v_new = f64::max(1.0, -brake * t + vc + ((-brake)*(-brake)*t*t - 2.0*(-brake)*dx + 2.0*(-brake)*t*vc).sqrt());
            let t_1 = (v_new-vc)/(-brake);
            let t_2 = (dx-0.5*(-brake)*t_1*t_1)/v_new;
            //print!("Calculated Travel time when braking to new speed {} is {} time units\n", v_new, t_1+t_2);

            return (v_new, t_min)
        }

    } else {
        if ((accel)*(accel)*t*t-2.0*(accel)*dx+2.0*(accel)*t*vc) < 0.0 {
            return (vmax, t_min)
        } else {
            let v_new = f64::min(vmax, accel * t + vc - (accel*accel*t*t - 2.0*accel*dx + 2.0*accel*t*vc).sqrt());
            let t_1 = (v_new - vc) / accel;
            let t_2 = (dx - 0.5 * accel * t_1 * t_1) / v_new;
            //print!("Calculated Travel time when accelerating to new speed {} is {} time units\n", v_new, t_1+t_2);
            return (v_new, t_min)
        }
    }
}

